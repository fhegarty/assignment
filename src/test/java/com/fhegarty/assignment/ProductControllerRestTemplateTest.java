package com.fhegarty.assignment;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.math.BigDecimal;

import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.fhegarty.assignment.model.entity.Product;
import com.fhegarty.assignment.service.ProductService;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class ProductControllerRestTemplateTest {

	@LocalServerPort
	private int port;

	@Autowired
	private TestRestTemplate restTemplate;
	
	@MockBean
	private ProductService mockProductService;

	
	@Test
	public void create_failure_emptyName_400() throws Exception {
		JSONObject productJsonObject = new JSONObject();
		productJsonObject.put("price", new BigDecimal(8.00));

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<String> requestEntity = new HttpEntity<>(productJsonObject.toString(), headers);

		ResponseEntity<String> response = restTemplate.postForEntity("/api/products", requestEntity, String.class);

		assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
		
		verify(mockProductService, times(0)).create(any(Product.class));
	}
	
	@Test
	public void create_failure_emptyPrice_400() throws Exception {
		JSONObject productJsonObject = new JSONObject();
		productJsonObject.put("name", "Product1");

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<String> requestEntity = new HttpEntity<>(productJsonObject.toString(), headers);

		ResponseEntity<String> response = restTemplate.postForEntity("/api/products", requestEntity, String.class);

		assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
		
		verify(mockProductService, times(0)).create(any(Product.class));
	}

}
