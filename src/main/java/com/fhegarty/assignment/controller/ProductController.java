package com.fhegarty.assignment.controller;

import java.net.URI;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.fhegarty.assignment.model.entity.Product;
import com.fhegarty.assignment.service.ProductService;

@RestController
@RequestMapping("/api/products")
public class ProductController {

	private final ProductService productService;

	public ProductController(ProductService productService) {
		this.productService = productService;
	}

	@PostMapping
	public ResponseEntity<Product> create(@Valid @RequestBody Product product) {
		Product productCreate = productService.create(product);

		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
				.buildAndExpand(productCreate.getId()).toUri();
		HttpHeaders headers = new HttpHeaders();
		headers.add("Location", location.toString());

		return new ResponseEntity<>(productCreate, headers, HttpStatus.CREATED);
	}

	@GetMapping
	public Iterable<Product> list() {
		return productService.listAllProducts();
	}

	@PutMapping("/{id}")
	public ResponseEntity<Product> update(@Valid @RequestBody Product product, @PathVariable Long id) {
		Optional<Product> productOptional = productService.findById(id);

		if (!productOptional.isPresent())
			return ResponseEntity.notFound().build();
		
		product.setId(id);
		
		productService.update(product);
		
		return ResponseEntity.ok(product);
	}

}
