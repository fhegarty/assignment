package com.fhegarty.assignment.controller;

import java.math.BigDecimal;
import java.net.URI;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import javax.validation.Valid;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.fhegarty.assignment.exception.ResourceNotFoundException;
import com.fhegarty.assignment.model.dto.OrderDto;
import com.fhegarty.assignment.model.dto.ProductDto;
import com.fhegarty.assignment.model.entity.Order;
import com.fhegarty.assignment.model.entity.Product;
import com.fhegarty.assignment.service.OrderService;
import com.fhegarty.assignment.service.ProductService;

@RestController
@RequestMapping("/api/orders")
public class OrderController {

	private final OrderService orderService;
	private final ProductService productService;

	public OrderController(OrderService orderService, ProductService productService) {
		this.orderService = orderService;
		this.productService = productService;
	}

	@PostMapping
	public ResponseEntity<Order> create(@Valid @RequestBody OrderDto orderDto) {

		Order orderCreate = orderService.create(convertOrderDtoToOrder(orderDto));

		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
				.buildAndExpand(orderCreate.getId()).toUri();
		HttpHeaders headers = new HttpHeaders();
		headers.add("Location", location.toString());

		return new ResponseEntity<>(orderCreate, headers, HttpStatus.CREATED);
	}

	private Order convertOrderDtoToOrder(OrderDto orderDto) {
		Set<Product> products = new HashSet<>();
		BigDecimal totalValue = BigDecimal.ZERO;

		for (ProductDto productDto : orderDto.getProducts()) {
			Optional<Product> productOptional = productService.findById(productDto.getId());

			if (!productOptional.isPresent())
				throw new ResourceNotFoundException("Product not found");
			
			Product product = productOptional.get();
			BigDecimal productPrice = product.getPrice();
			Integer quantity = productDto.getQuantity();
			totalValue = totalValue.add(productPrice.multiply(new BigDecimal(quantity)));
			
			products.add(product);
		}

		return new Order(orderDto.getEmail(), products, totalValue);
	}

}
