package com.fhegarty.assignment.model.dto;

import java.util.List;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

public class OrderDto {

	@NotEmpty(message = "Please provide your email")
	@Email(message = "Email must be valid")
	private String email;

	@Size(min = 1, message = "Please add products to the order")
	private List<ProductDto> products;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public List<ProductDto> getProducts() {
		return products;
	}

	public void setProducts(List<ProductDto> products) {
		this.products = products;
	}

}
