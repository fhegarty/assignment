package com.fhegarty.assignment.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.fhegarty.assignment.model.entity.Product;

@Repository
public interface ProductRepository extends CrudRepository<Product, Long> {

}
