package com.fhegarty.assignment.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.fhegarty.assignment.model.entity.Order;

@Repository
public interface OrderRepository extends CrudRepository<Order, Long> {

}
