package com.fhegarty.assignment.service;

import java.util.Optional;

import com.fhegarty.assignment.model.entity.Product;

public interface ProductService {
	
	Product create(Product product);
	
	Iterable<Product> listAllProducts();

	Optional<Product> findById(Long id);

	Product update(Product product);

}
