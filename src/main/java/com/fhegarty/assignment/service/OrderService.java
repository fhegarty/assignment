package com.fhegarty.assignment.service;

import com.fhegarty.assignment.model.entity.Order;

public interface OrderService {

	Order create(Order order);
}
