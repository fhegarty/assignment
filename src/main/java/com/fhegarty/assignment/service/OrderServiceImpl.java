package com.fhegarty.assignment.service;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.fhegarty.assignment.model.entity.Order;
import com.fhegarty.assignment.repository.OrderRepository;

@Service
@Transactional
public class OrderServiceImpl implements OrderService {

	private final OrderRepository orderRepository;

	public OrderServiceImpl(OrderRepository orderRepository) {
		this.orderRepository = orderRepository;
	}

	@Override
	public Order create(Order order) {
		return orderRepository.save(order);
	}

}
