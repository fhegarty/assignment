# Assignment
Spring Boot application includes embedded Tomcat, H2 database and Swagger.

## Requirements
* JDK 11
* Maven 3

## How to Run
* Run ```mvn:spring-boot run```
* Or, ```mvn clean package``` and run the executable jar.

## REST-API documentation
Run the application and browse to http://localhost:8080/swagger-ui.html

## Considerations
* I propose JWT OAuth2 for authentication of the web service. Spring Boot and Spring Security permit a fast and easy setup of a OAuth2 authorization/authentication server.
* To make the service redundant, have multiple service instances running with a load balancer. The number of service instances depends on the expected load on the service. A stateless service is ideal for scaling (no HttpSession).
